<?php
/*class NuSoapServer extends CI_Controller {
	function __construct() {
		parent::__construct();
		error_reporting(0);
		
		$this->load->library("nuSoap_lib");
		$this->nusoap_server = new soap_server();
		$this->nusoap_server->configureWSDL("cartWSDL", "urn:cartWSDL");
		
		$this->nusoap_server->wsdl->addComplexType(
			"News",
			"complexType",
			"struct",
			"sequence",
			"",
			array(
				"id"=>array("name"=>"id", "type"=>"xsd:int"),
				"title"=>array("name"=>"title", "type"=>"xsd:string"),
				"slug"=>array("name"=>"slug", "type"=>"xsd:string"),
				"text"=>array("name"=>"text", "type"=>"xsd:string")
				)
			);
		$this->nusoap_server->register(
			"getNews",
			array(
			"slug" => "xsd:string",
		),
		array("news"=>"tns:News"),
			"urn:cartWSDL",
			"urn:cartWSDL#getNews",
			"rpc",
			"encoded",
			"Returns all news."
		);


		$this->nusoap_server->register(
			"authenticateUser",
			array(
			"uname" => "xsd:string",
			"pass" => "xsd:string",
		),
		array("success"=>"xsd:string"),
			"urn:cartWSDL",
			"urn:cartWSDL#authenticateUser",
			"rpc",
			"encoded",
			"Return true if authentication successfull else false."
		);

		
	}
	function index() {
		if($this->uri->segment(3) == "wsdl") {
			$_SERVER['QUERY_STRING'] = "wsdl";
		} else {
			$_SERVER['QUERY_STRING'] = "";
		}
		$this->nusoap_server->service(file_get_contents("php://input"));
	}
	

	function get_news() {
		function getNews($slug) {
			//echo "True";
			$CI =& get_instance();
			$CI->load->model("news_model");
			
			$data['news_item'] = $CI->news_model->get_news_where($slug);
			return $data['news_item'];
		}
		$this->nusoap_server->service(file_get_contents("php://input"));
	}

	function authenticate_user() {
		function authenticateUser($uname, $pass) {
			//echo "True";
			$CI =& get_instance();
			$CI->load->model("login_database");
			
			$data = array(
			'username' => $uname,
			'password' => $pass
			);
			$result = $CI->login_database->login($data);
			if($result == 1)
				return "True";
			else 
				return "False";
			return $result;
		}
		$this->nusoap_server->service(file_get_contents("php://input"));
	}
}*/

class NuSoapServer extends CI_Controller {
	function __construct() {
		parent::__construct();
		error_reporting(0);
		
		
		$this->load->library("nuSoap_lib");
		$this->nusoap_server = new soap_server();

		
		$this->nusoap_server->configureWSDL("cartWSDL", "urn:cartWSDL","https://www.studenti.famnit.upr.si/~klen/soap_ci/Codeigniter/index.php/nuSoapServer/index");
		
		$this->nusoap_server->wsdl->addComplexType(
			"News",
			"complexType",
			"struct",
			"sequence",
			"",
			array(
				"id"=>array("name"=>"id", "type"=>"xsd:int"),
				"title"=>array("name"=>"title", "type"=>"xsd:string"),
				"slug"=>array("name"=>"slug", "type"=>"xsd:string"),
				"text"=>array("name"=>"text", "type"=>"xsd:string")
				)
			);
		
		$this->nusoap_server->wsdl->addComplexType(
			"Status",
			"complexType",
			"struct",
			"sequence",
			"",
			array(
				"error"=>array("name"=>"email", "type"=>"xsd:string"),
				"added"=>array("name"=>"success", "type"=>"xsd:boolean")
				)
			);
		


		$this->nusoap_server->register(
			"getNews",
			array(
			"slug" => "xsd:string",
		),
		array("news"=>"tns:News"),
			"urn:cartWSDL",
			"urn:cartWSDL#getNews",
			"rpc",
			"encoded",
			"Returns all news."
		);


		$this->nusoap_server->register(
			"authenticateUser",
			array(
			"uname" => "xsd:string",
			"pass" => "xsd:string",
		),
		array("success"=>"xsd:boolean"),
			"urn:cartWSDL",
			"urn:cartWSDL#authenticateUser",
			"rpc",
			"encoded",
			"Return true if authentication successfull else false."
		);

		$this->nusoap_server->register(
			"signUp",
			array(
			"uname" => "xsd:string",
			"pass" => "xsd:string",
			"email" => "xsd:string"
		),
		array("status"=>"tns:Status"),
			"urn:cartWSDL",
			"urn:cartWSDL#signUp",
			"rpc",
			"encoded",
			"Return true if user added to database successfully."
		);

		
	}
	
	function index() {

		if(isset($_GET['wsdl'])) {
			$_SERVER['QUERY_STRING'] = "wsdl";
		} else {
			$_SERVER['QUERY_STRING'] = "";
		}

		function getNews($slug) {
			//echo "True";
			$CI =& get_instance();
			$CI->load->model("news_mosoapdel");
			
			$data['news_item'] = $CI->news_model->get_news_where($slug);
			return $data['news_item'];
		}


		function authenticateUser($uname, $pass) {
			//echo "True";
			$CI =& get_instance();
			$CI->load->model("login_database");
			
			$data = array(
			'username' => $uname,
			'password' => $pass
			);
			$result = $CI->login_database->login($data);
			return $result;
		}

		function signUp($uname, $email, $pass){
			$CI =& get_instance();
			$CI->load->model("login_database");
			
			$data = array(
				'user_name' => $uname,
				'user_email' => $email,
				'user_password' => $pass
			);
			$result = $CI->login_database->registration_insert($data);

			$response = array(
				'error' => "No errors reported.",
				'added' => $result);

			if($result == 0)
				$response['error'] = "User already exists.";
			
			
			return $response;

		}


	
		$this->nusoap_server->service(file_get_contents("php://input"));
	}
	


}
?>
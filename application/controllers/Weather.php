<?php
class Weather extends CI_Controller {

        public function index($page = 'home')
		{
		// Turn off all error reporting
        error_reporting(0);

        	$this->load->helper('url_helper');
			$this->load->library("NuSoap_lib");

            header('Content-type: text/xml');
            echo '<?xml version="1.0"?><?xml-stylesheet type="text/xsl" href="https://www.studenti.famnit.upr.si/~klen/soap_ci/Codeigniter/assets/weather.xsl"?><for>';

            $weather = new NuSoap_lib();
            echo $weather->getWether("40.785091", "-73.968285")->data->asXml();

            echo "</for>";
            // $this->load->view('templates/header');
            // $this->load->view('weather/index');
            // $this->load->view('templates/footer');

			
		}

}
<script type="text/javascript">
	function soap() {
		console.log("soap()");
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', 'https://graphical.weather.gov:443/xml/SOAP_server/ndfdXMLserver.php', true);

        // build SOAP request
        
        var body= '<?php echo'<?xml version="1.0"?>' ?> \
            <soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope/" \
  					soap:encodingStyle="http://www.w3.org/2003/05/soap-encoding" \
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> \
  <soap:Body> \
    <NDFDgen> \
      <latitude>40.785091</latitude> \
      <longitude>-73.968285</longitude> \
      <product>time-series</product> \
      <startTime>2020-04-27T08:00:00</startTime> \
      <endTime>2020-05-01T08:00:00</endTime> \
      <Unit></Unit> \
      <weatherParameters> \
        <maxt xsi:type="xsd:boolean">1</maxt> \
        <mint xsi:type="xsd:boolean">1</mint> \
        <temp xsi:type="xsd:boolean">0</temp> \
        <td xsi:type="xsd:boolean">0</td> \
        <pop12 xsi:type="xsd:boolean">0</pop12> \
        <qpf xsi:type="xsd:boolean">0</qpf> \
        <sky xsi:type="xsd:boolean">1</sky> \
        <snow xsi:type="xsd:boolean">0</snow> \
        <wspd xsi:type="xsd:boolean">0</wspd> \
        <wdir xsi:type="xsd:boolean">0</wdir> \
        <wx xsi:type="xsd:boolean">0</wx> \
        <waveh xsi:type="xsd:boolean">0</waveh> \
        <icons xsi:type="xsd:boolean">1</icons> \
        <rhm xsi:type="xsd:boolean">0</rhm> \
        <apt xsi:type="xsd:boolean">0</apt> \
        <tcwspdabv34i xsi:type="xsd:boolean">0</tcwspdabv34i> \
        <tcwspdabv50i xsi:type="xsd:boolean">0</tcwspdabv50i> \
        <tcwspdabv64i xsi:type="xsd:boolean">0</tcwspdabv64i> \
        <tcwspdabv34c xsi:type="xsd:boolean">0</tcwspdabv34c> \
        <tcwspdabv50c xsi:type="xsd:boolean">0</tcwspdabv50c> \
        <tcwspdabv64c xsi:type="xsd:boolean">0</tcwspdabv64c> \
        <conhazo xsi:type="xsd:boolean">0</conhazo> \
        <ptornado xsi:type="xsd:boolean">0</ptornado> \
        <phail xsi:type="xsd:boolean">0</phail> \
        <ptstmwinds xsi:type="xsd:boolean">0</ptstmwinds> \
        <pxtornado xsi:type="xsd:boolean">0</pxtornado> \
        <pxhail xsi:type="xsd:boolean">0</pxhail> \
        <pxtstmwinds xsi:type="xsd:boolean">0</pxtstmwinds> \
        <ptotsvrtstm xsi:type="xsd:boolean">0</ptotsvrtstm> \
        <ptotxsvrtstm xsi:type="xsd:boolean">0</ptotxsvrtstm> \
        <tmpabv14d xsi:type="xsd:boolean">0</tmpabv14d> \
        <tmpblw14d xsi:type="xsd:boolean">0</tmpblw14d> \
        <tmpabv30d xsi:type="xsd:boolean">0</tmpabv30d> \
        <tmpblw30d xsi:type="xsd:boolean">0</tmpblw30d> \
        <tmpabv90d xsi:type="xsd:boolean">0</tmpabv90d> \
        <tmpblw90d xsi:type="xsd:boolean">0</tmpblw90d> \
        <prcpabv14d xsi:type="xsd:boolean">0</prcpabv14d> \
        <prcpblw14d xsi:type="xsd:boolean">0</prcpblw14d> \
        <prcpabv30d xsi:type="xsd:boolean">0</prcpabv30d> \
        <prcpblw30d xsi:type="xsd:boolean">0</prcpblw30d> \
        <prcpabv90d xsi:type="xsd:boolean">0</prcpabv90d> \
        <prcpblw90d xsi:type="xsd:boolean">0</prcpblw90d> \
        <precipa_r xsi:type="xsd:boolean">0</precipa_r> \
        <sky_r xsi:type="xsd:boolean">0</sky_r> \
        <td_r xsi:type="xsd:boolean">0</td_r> \
        <temp_r xsi:type="xsd:boolean">0</temp_r> \
        <wdir_r xsi:type="xsd:boolean">0</wdir_r> \
        <wspd_r xsi:type="xsd:boolean">0</wspd_r> \
        <wgust xsi:type="xsd:boolean">0</wgust> \
      </weatherParameters> \
    </NDFDgen> \
  </soap:Body> \
</soap:Envelope>';

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4) {
                if (xmlhttp.status == 200) {
                    alert(xmlhttp.responseText);
                    // alert('done. use firebug/console to see network response');
                }
            }
        }
        // Send the POST request
        xmlhttp.setRequestHeader('Content-Type', 'text/xml');
        xmlhttp.send(body);
        // send request
        // ...
    }

  soap();
</script>
<h3>Weather forecast</h3>
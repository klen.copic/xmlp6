<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template match="/">
  <html>
    <head>
      <link rel="stylesheet" type="text/css" href="https://www.studenti.famnit.upr.si/~klen/soap_ci/Codeigniter/assets/mystyle.css"/>
    </head>

  <body>
  
  <xsl:for-each select="for/data">
  <h2>Weather</h2>
  <table border="0">
    <tr bgcolor="#9acd32">
      <th>Time1</th>
      <th>Icon</th>
      <th>Temperature</th>
      <th>Temperature Min</th>
      <th>Temperature Max</th>
    </tr>

    <tr>
    <td>
      <xsl:for-each select="time-layout[layout-key='k-p3h-n16-3']">
          <xsl:for-each select="start-valid-time"><div><xsl:value-of select="."/></div>
          </xsl:for-each>
      </xsl:for-each>
    </td>
    <xsl:for-each select="parameters/conditions-icon">
        <td><xsl:for-each select="icon-link"><div><img src="{.}"/></div></xsl:for-each></td>
    </xsl:for-each>
    
    <xsl:for-each select="parameters/temperature[@type='hourly']">
            <td>
            <xsl:for-each select="value"><div><xsl:value-of select="."/></div></xsl:for-each></td>
    </xsl:for-each>
    
    <xsl:for-each select="parameters/temperature[@type='minimum']">
      <!--td>
        <xsl:value-of select="name"/></td-->
        <td><xsl:for-each select="value">
          <xsl:value-of select="."/><br />    
        </xsl:for-each></td>
    </xsl:for-each>

    <xsl:for-each select="parameters/temperature[@type='maximum']">
        <td><xsl:for-each select="value">
          <xsl:value-of select="."/><br />    
        </xsl:for-each></td>
    </xsl:for-each>
    
    
    
   </tr>
   </table>
 </xsl:for-each>

  </body>
  </html>
</xsl:template>

</xsl:stylesheet>